﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShowArticles.aspx.cs" Inherits="ShowArticles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    <div style="background-image: url('bc3.png'); background-attachment: fixed; min-height:1000px; height: auto; width: 100%;">
        <div style="padding-top:350px; padding-bottom:100px;">
          
    <div  class="articleContainer2" id="showArticles" runat="server">
        <div style="text-align:center;">
            <div id="titlepage" runat="server" style="margin-top:10px;">
              <asp:Label style="color:black; font-size:larger;" ID="Label1" runat="server" Text="Papers about "></asp:Label>
                </div>
            <br />
            <div style="margin-bottom:10px">
                <asp:Label ID="Label2" runat="server" Text="Number of Appearances per Year"></asp:Label>
                <br />
                <br />
         <svg id="barChart"></svg>
          

         <script>

             var marginb = { topb: 20, rightb: 20, bottomb: 70, leftb: 40 },
                 widthb = 600 - marginb.leftb - marginb.rightb,
                 heightb = 300 - marginb.topb - marginb.bottomb;

             // Parse the date / time
            // var parseDate = d3.time.format("%Y-%m").parse;

             var xb = d3.scale.ordinal().rangeRoundBands([0, widthb], .2);

             var yb = d3.scale.linear().range([heightb, 0]);

             var xAxisb = d3.svg.axis()
                 .scale(xb)
                 .orient("bottom")
               //  .tickFormat(d3.time.format("%Y-%m"));

             var yAxisb = d3.svg.axis()
                 .scale(yb)
                 .orient("left")
                 .ticks(10);

             var barChart = d3.select("#barChart")
                 .attr("width", widthb + marginb.leftb + marginb.rightb)
                 .attr("height", heightb + marginb.topb + marginb.bottomb)
                 .append("g")
                 .attr("transform",
                 "translate(" + marginb.leftb + "," + marginb.topb + ")");

             d3.csv("bardata.txt", function (error, data) {

                 data.forEach(function (d) {
                     d.date = d.date;
                     d.value = d.value;
                 });

                 xb.domain(data.map(function (d) { return d.date; }));
                 yb.domain([0, d3.max(data, function (d) { return d.value; })]);

                 barChart.append("g")
                     .attr("class", "x axis")
                     .attr("transform", "translate(0," + heightb + ")")
                     .call(xAxisb)
                     .selectAll("text")
                     .style("text-anchor", "end")
                     .attr("dx", "-.8em")
                     .attr("dy", "-.55em")
                     .attr("transform", "rotate(-90)");

                 barChart.append("g")
                     .attr("class", "y axis")
                     .call(yAxisb)
                     .append("text")
                     .attr("transform", "rotate(-90)")
                     .attr("y", 6)
                     .attr("dy", ".71em")
                     .style("text-anchor", "end")
                     .text("Value ($)");

                 barChart.selectAll("bar")
                     .data(data)
                     .enter().append("rect")
                     .style("fill", "#98abc5")
                     .attr("x", function (d) { return xb(d.date); })
                     .attr("width", xb.rangeBand())
                     .attr("y", function (d) { return yb(d.value); })
                     .attr("height", function (d) { return heightb - yb(d.value); });

             });

</script>
            <br />
            
            <br /><br />

            </div>
        <div>
            <style>


path.slice{
	stroke-width:2px;
}

polyline{
	opacity: .3;
	stroke: black;
	stroke-width: 2px;
	fill: none;
}

#pieChart {
    
}

</style>
             <asp:Label ID="Label3" runat="server" Text="Percentage of Appearances per Conference"></asp:Label>
            <br />
       <svg  id="pieChart" width="960" height="500"></svg>      
       <script>

           var svg = d3.select("#pieChart")
                 .append("g")

             svg.append("g")
                 .attr("class", "slices");
             svg.append("g")
                 .attr("class", "labels");
             svg.append("g")
                 .attr("class", "lines");

             var width = 960,
                 height = 450,
                 radius = Math.min(width, height) / 2;

             var pie = d3.layout.pie()
                 .sort(null)
                 .value(function (d) {
                     return d.value;
                 });

             var arc = d3.svg.arc()
                 .outerRadius(radius * 0.8)
                 .innerRadius(radius * 0.4);

             var outerArc = d3.svg.arc()
                 .innerRadius(radius * 0.9)
                 .outerRadius(radius * 0.9);

             svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

             var key = function (d) { return d.data.label; };

             var color = d3.scale.ordinal()
                 .domain(["ACL", "CVPR", "NIPS"])
                 .range(["#98abc5", "#a05d56", "#6b486b"]);


             d3.csv("bardataC.txt", function (error, data) {

                 data.forEach(function (d) {
                     d.label = d.label;
                     d.value = d.value;
                 });
             
             //     change(data);

        /*     function randomData() {
                 var labels = color.domain();
                 return labels.map(function (label) {
                     return { label: label, value: Math.random() }
                 });
             }*/

            

            


           //  function change(data) {

                 /* ------- PIE SLICES -------*/
                var slice = svg.select(".slices").selectAll("path.slice")
                     .data(pie(data), key);

                 slice.enter()
                     .insert("path")
                     .style("fill", function (d) { return color(d.data.label); })
                     .attr("class", "slice");

                 slice
                     .transition().duration(1000)
                     .attrTween("d", function (d) {
                         this._current = this._current || d;
                         var interpolate = d3.interpolate(this._current, d);
                         this._current = interpolate(0);
                         return function (t) {
                             return arc(interpolate(t));
                         };
                     })

                 slice.exit()
                     .remove();

                 /* ------- TEXT LABELS -------*/

                var text = svg.select(".labels").selectAll("text")
                     .data(pie(data), key);

                 text.enter()
                     .append("text")
                     .attr("dy", ".35em")
                     .text(function (d) {
                         return d.data.label + " - " +d.data.value+"%";
                     });

                 function midAngle(d) {
                     return d.startAngle + (d.endAngle - d.startAngle) / 2;
                 }

                 text.transition().duration(1000)
                     .attrTween("transform", function (d) {
                         this._current = this._current || d;
                         var interpolate = d3.interpolate(this._current, d);
                         this._current = interpolate(0);
                         return function (t) {
                             var d2 = interpolate(t);
                             var pos = outerArc.centroid(d2);
                             pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                             return "translate(" + pos + ")";
                         };
                     })
                     .styleTween("text-anchor", function (d) {
                         this._current = this._current || d;
                         var interpolate = d3.interpolate(this._current, d);
                         this._current = interpolate(0);
                         return function (t) {
                             var d2 = interpolate(t);
                             return midAngle(d2) < Math.PI ? "start" : "end";
                         };
                     });

                 text.exit()
                     .remove();

                 /* ------- SLICE TO TEXT POLYLINES -------*/

                 var polyline = svg.select(".lines").selectAll("polyline")
                     .data(pie(data), key);

                 polyline.enter()
                     .append("polyline");

                 polyline.transition().duration(1000)
                     .attrTween("points", function (d) {
                         this._current = this._current || d;
                         var interpolate = d3.interpolate(this._current, d);
                         this._current = interpolate(0);
                         return function (t) {
                             var d2 = interpolate(t);
                             var pos = outerArc.centroid(d2);
                             pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                             return [arc.centroid(d2), outerArc.centroid(d2), pos];
                         };
                     });

                 polyline.exit()
                     .remove();
          //   };
             });
</script>
            <br />
           
          
            </div>
       <br />
      
     
    </div>
            </div>
     </div>
</asp:Content>

