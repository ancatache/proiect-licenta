﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UpdateKeywords : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SintagmHelper sintagmHelper = SintagmHelper.getInstance();
        sintagmHelper.deleteAllSintagms();

        Dictionary<String, List<int>> voc = WordUtils.calculateVocabular();
        ArticleHelper articleHelper1 = ArticleHelper.getInstance();
        List<Article> articles = articleHelper1.getAllArticles();
        for (int i = 0; i < articles.Count; i++)
        {
            Dictionary<String, Double> tfidf_values = WordUtils.calculateTFIDF(i, voc);
            Dictionary<String, Double> twoWordsSintagms = articles[i].getTwoWordsSintagms(tfidf_values);
            Dictionary<String, Double> threeWordsSintagms = articles[i].getThreeWordsSintagms(tfidf_values);
            Dictionary<String, Double> oneSingleDictionary = new Dictionary<String, Double>(); // combines the two dictionaries of two respective three words 
            twoWordsSintagms.ToList().ForEach(x => oneSingleDictionary.Add(x.Key, x.Value));
            threeWordsSintagms.ToList().ForEach(x => oneSingleDictionary.Add(x.Key, x.Value));
            Sintagm[] s = articles[i].getMostPopularThreeSintagms(oneSingleDictionary);

            BaseDBResponse response;
            for (int j = 0; j < s.Length; j++)
                response = sintagmHelper.saveSintagm(s[j]);
        }
        Response.Redirect("Home.aspx", false);
    }
}
 