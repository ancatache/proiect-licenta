﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ShowArticles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string path = HttpContext.Current.Request.Url.Query;
            path = path.Substring(1, path.Length - 1);
            int ok = 0;
            String[] words = path.Split(new string[] { "%20" }, StringSplitOptions.None);
            Dictionary<String, List<int>> voc = WordUtils.calculateVocabular();
            ArticleHelper articleHelper1 = ArticleHelper.getInstance();
            List<Article> articles = articleHelper1.getAllArticles();
            Label label2 = new Label();
            double nrACL=0, nrCVPR=0, nrNIPS=0, nr2008=0, nr2012=0, nr2016=0;
            for (int j = 0; j < words.Length; j++)
                label2.Text = label2.Text + words[j] + " ";
            label2.Text = label2.Text + "</br></br>";
            label2.CssClass = "keyword";
            titlepage.Controls.Add(label2);
            for (int j = 0; j < articles.Count; j++)
            {
                

                for (int i = 0; i < words.Length; i++)
                {
                    List<int> art = voc[words[i]];
                    if (art[j] > 3)
                    {
                        switch (articles[j].category)
                        {
                            case "ACL":
                                nrACL++;
                                break;
                            case "NIPS":
                                nrNIPS++;
                                break;
                            case "CVPR":
                                nrCVPR++;
                                break;
                        }
                        switch (articles[j].year)
                        {
                            case 2008:
                                nr2008++;
                                break;
                            case 2012:
                                nr2012++;
                                break;
                            case 2016:
                                nr2016++;
                                break;
                        }
                        HyperLink hypLink = new HyperLink();
                        hypLink.Text = articles[j].title + "</br>" + "(" + articles[j].year + " - " + articles[j].category + ")" + "<br /><br />";
                        hypLink.NavigateUrl = "~/ArticleView.aspx?" + articles[j].Id;
                        hypLink.CssClass = "hiper";
                        showArticles.Controls.Add(hypLink);
                        //  showArticles.Controls.Add("</br>");
                        break;
                    }

                }
            }

            double sum = nrNIPS + nrCVPR + nrACL;
            nrNIPS = Math.Round(nrNIPS / sum * 100);
            nrCVPR = Math.Round(nrCVPR / sum * 100);
            nrACL = Math.Round(nrACL / sum * 100);
            string lines = "label,value\r\nACL,"+nrACL+"\r\nNIPS,"+nrNIPS+ "\r\nCVPR,"+nrCVPR;
            System.IO.StreamWriter bardataC = new System.IO.StreamWriter("D:\\Users\\Sebi\\Documents\\proiect-licenta\\bardataC.txt");
            bardataC.WriteLine(lines);
            bardataC.Close();

            string lines1 = "date,value\r\n2008," + nr2008 + "\r\n2012," + nr2012 + "\r\n2016," + nr2016;
            System.IO.StreamWriter bardata = new System.IO.StreamWriter("D:\\Users\\Sebi\\Documents\\proiect-licenta\\bardata.txt");
            bardata.WriteLine(lines1);
            bardata.Close();



        }
    }
}