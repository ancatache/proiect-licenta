﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:SqlDataSource ID="SDSSearch" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand="SELECT Id, title, description, year,category FROM Article">
    </asp:SqlDataSource>
     <asp:Label ID="LSelect" runat="server" Text=""></asp:Label>
    <br />
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SDSSearch">
        <HeaderTemplate>
            <div style="width: 70%; margin:auto; padding-bottom:50px;  padding-right:250px; font-size: 150%;">Search results:</div>
        </HeaderTemplate>
        <ItemTemplate>
            <div class="articleContainer" style="width: 70%; margin:auto; padding-bottom:50px;  padding-right:250px;">
                <div style="padding: 2px;  ">
                    <asp:HyperLink style="color: #FF4081; text-decoration:none; font-size: 200%;" ID="HyperLink3" NavigateUrl='<%#"~/ArticleView.aspx?"+ Eval("Id") %>' runat="server"><%#Eval("title") %></asp:HyperLink>
                    <br />
                    <asp:Label ID="LabelYear" runat="server" Text='<%#"("+Eval("year")+")" %>'></asp:Label>
                    <br />
                    <asp:Label ID="LabelC" runat="server" Text='<%#"("+Eval("category")+")" %>'></asp:Label>
                </div>
                <div style="overflow:hidden; padding: 5px; ; margin-top:10px; height: 220px;">
                    <img runat="server" id="ArticleImg1" style="width:300px; height:auto; float: right; padding-right: 5px; padding-bottom: 5px;" src='<%#"~/Images/"+Eval("Id")+".jpg"%>' />
                    <div style=""><%#Eval("description")+"..."%></div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>

