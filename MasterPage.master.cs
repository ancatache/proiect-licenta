﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{

    

    protected void clickLogo(object sender, EventArgs e)
    {
        Response.Redirect("Home.aspx", false);
    }

    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {

        CreateUserWizard lg = (CreateUserWizard)LoginView1.Controls[0].FindControl("CreateUserWizard1");
        Roles.AddUserToRole(lg.UserName, "User");
    }
    protected void CreateUserWizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        CreateUserWizard lg = (CreateUserWizard)LoginView1.Controls[0].FindControl("CreateUserWizard1");
        WizardStep wz = (WizardStep)lg.FindControl("Wizard");

        try
        {
            Profile.FirstName = ((TextBox)wz.FindControl("TBFirstName")).Text;
            Profile.LastName = ((TextBox)wz.FindControl("TBLastName")).Text;
            Profile.Birthday = DateTime.Parse(((TextBox)wz.FindControl("TBBirthday")).Text);
            Profile.Gender = int.Parse(((RadioButtonList)wz.FindControl("RBLGender")).SelectedValue);
        }
        catch (Exception ex)
        {

        }
    }
    protected void CreateUserWizard1_ContinueButtonClick(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Response.Redirect("Home.aspx", false);
        }
    }


}


