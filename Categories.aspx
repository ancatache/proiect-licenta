﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Categories.aspx.cs" Inherits="Categories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="width: 70%; margin:auto; padding-bottom:20px; padding-top:20px;  padding-right:250px;">
        <asp:Label ID="Label1" runat="server" Text="Sort:"></asp:Label>
        <asp:DropDownList style="margin-bottom:20px;" ID="DropDownList1" runat="server">
            <asp:ListItem Value="Year"> Year </asp:ListItem>
            <asp:ListItem Value="Alphabetically"> Alphabetically </asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="Button1" runat="server" Text="Sort" OnClick="Sort"/>
    </div>
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            <div class="articleContainer" style="width: 70%; margin:auto; padding-bottom:50px;  padding-right:250px;">
                <div style="padding: 2px; ">
                    <asp:HyperLink style="color: #FF4081; text-decoration:none; font-size: 200%;" ID="HyperLink3" NavigateUrl='<%#"~/ArticleView.aspx?"+ Eval("Id") %>' runat="server"><%#Eval("title") %></asp:HyperLink>
                    <br />
                    <asp:Label ID="LabelYear" runat="server" Text='<%#"("+Eval("year")+")" %>'></asp:Label>
                    <br />
                    <asp:Label ID="LabelC" runat="server" Text='<%#"("+Eval("category")+")" %>'></asp:Label>
                </div>
                <div style="overflow:hidden; padding: 5px;  margin-top:10px; height: 220px;">
                    <img runat="server" id="ArticleImg1" style="width:300px; height:auto; float: right; padding-right: 5px; padding-bottom: 5px;" src='<%#"~/Images/"+Eval("Id")+".jpg"%>' />
                    <div><%#Eval("description")+"..."%></div>
                </div>
            </div>
        </ItemTemplate>
        <SeparatorTemplate>
            <br />
        </SeparatorTemplate>
    </asp:Repeater>
      <asp:Repeater ID="Repeater2" runat="server" DataSourceID="SqlDataSource2">
        <ItemTemplate>
            <div class="articleContainer" style="width: 70%; margin:auto; padding-bottom:50px;  padding-right:250px;">
                <div style="padding: 2px;  ">
                    <asp:HyperLink style="color: #FF4081; text-decoration:none; font-size: 200%;" ID="HyperLink3" NavigateUrl='<%# Eval("URL") %>' runat="server"><%#Eval("title") %></asp:HyperLink>
                    <br />
                    <asp:Label ID="LabelYear" runat="server" Text='<%#"("+Eval("year")+")" %>'></asp:Label>
                    <br />
                    <asp:Label ID="LabelC" runat="server" Text='<%#"("+Eval("category")+")" %>'></asp:Label>
                </div>
                <div style="overflow:hidden; padding: 5px;  margin-top:10px; height: 220px;">
                    <img runat="server" id="ArticleImg1" style="width:300px; height:auto; float: right; padding-right: 5px; padding-bottom: 5px;" src='<%#"~/Images/"+Eval("Id")+".jpg"%>' />
                    <div><%#Eval("description")+"..."%></div>
                </div>
            </div>
        </ItemTemplate>
        <SeparatorTemplate>
            <br />
        </SeparatorTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand=''>
    </asp:SqlDataSource>
     <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand=''>
    </asp:SqlDataSource>
</asp:Content>

