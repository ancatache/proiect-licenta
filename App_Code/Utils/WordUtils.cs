﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;


/// <summary>
/// Summary description for WordUtils
/// </summary>
public class WordUtils
{
    private static Dictionary<String, bool> stopWordsDictionary;


    private static Dictionary<String, bool> makeStopWords()
    {
        stopWordsDictionary = new Dictionary<String, bool>();
        char[] Chars = { '\n' };
        String stopWords = System.IO.File.ReadAllText(@"D:\Users\Sebi\Documents\proiect-licenta\App_Data\stopwords-anca-1.txt");
        String[] stopArray = stopWords.Split(Chars, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < stopArray.Length; i++)
        {
            if (!stopWordsDictionary.ContainsKey(stopArray[i]))
                stopWordsDictionary.Add(stopArray[i], true);
        }
        return stopWordsDictionary;
    }

    public static Boolean isFromCharacters(string word)
    {
        int ok = 0;
        for (int i = 0; i < word.Length; i++)
            if (word[i] < 'a' || word[i] > 'z')
                ok = 1;
        if(ok==1)
            return false;
        return true;
    }

    public static Dictionary<String, bool> getStopWords()
    {
      //  if (stopWordsDictionary == null)
            return makeStopWords();
      //  else
      //      return stopWordsDictionary;
    }

    public static String removeSpecialCharacters(String str)
    {
        StringBuilder sb = new StringBuilder();
        foreach (char c in str)
        {
            if (char.IsLetter(c))
            {
                sb.Append(c);
            }
            else if(c == '-')
                sb.Append(c);
            else sb.Append(' ');
        }
        return sb.ToString();
    }

    public static String[] splitText(String text)
    {
        char[] delimiterChars = { ' ' };
        return text.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
    }

 /*   public static double termFrequency1(String word, Article article)
    {
        Dictionary<String, int> dictionary = article.getWords();
        int numar = article.numberOfAppearance(word);
        double rez = Math.Log(article.numberOfAppearance(word), 2.0);
        return 1 + rez; 
    }*/

    public static double termFrequency(int nrOfAppearance)
    {
 
        double rez = Math.Log(nrOfAppearance, 2.0);
        return 1 + rez;
    }

  /*  public static double inverseDocumentFrequency1(String word, List<Article> articles)
    {
        int nr=0;
        for(int i=0;i<articles.Count;i++)
            if(articles[i].containsWord(word.ToLower()))
                nr++;
        
        return Math.Log( (articles.Count) / nr, 2.0);
    }*/

    public static double inverseDocumentFrequency(int totalArt, int nrApar )
    {

        return Math.Log((totalArt) / nrApar, 2.0);
    }

    public static String[] splitSintagm(String sintagm)
    {
        char[] delimiterChars = { ' ' };
        return sintagm.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);
    }

    public static Dictionary<String, Double> normalization(Dictionary<String, Double> words) // normalizes the tfidf values of words from an article raported to maximum tfidf value from that article
    {
        double max = words.Values.Max();
        List<string> keys = new List<string>(words.Keys);
        foreach (String key in keys)
            words[key] = words[key] / max;
        return words;
    }

    public static Dictionary<String, List<int>> calculateVocabularForTopics(int year, string conference)
    {
        ArticleHelper articleHelper1 = ArticleHelper.getInstance();
        List<Article> articles = articleHelper1.getAllArticlesByYearAndConference(year, conference);
        Dictionary<String, List<int>> vocabular = new Dictionary<string, List<int>>();
        for (int i = 0; i < articles.Count; i++)
        {

            Dictionary<String, int> words = articles[i].getWords(); // iau lista de cuvinte fara stopwords
            foreach (String key in words.Keys) // pentru fiecare cuvant
            {

                if (!vocabular.ContainsKey(key))
                {
                    vocabular.Add(key, new List<int>());
                    for (int j = 0; j <= articles.Count; j++)
                        vocabular[key].Add(0);
                    vocabular[key][i] = words[key];

                }
                else

                    vocabular[key][i] = words[key];

            }
        }

        foreach (String key in vocabular.Keys) // pentru fiecare cuvant
        {
            int nr = 0;
            for (int z = 0; z < vocabular[key].Count; z++)
                if (vocabular[key][z] != 0)
                    nr++;
            vocabular[key][articles.Count] = nr;
        }
        return vocabular;
    }

    

    public static Dictionary<String, List<int>> calculateVocabular()
    {
        ArticleHelper articleHelper1 = ArticleHelper.getInstance();
        List<Article> articles = articleHelper1.getAllArticles();
        Dictionary<String, List<int>> vocabular = new Dictionary<string, List<int>>();
        for (int i = 0; i < articles.Count; i++)
        {

            Dictionary<String, int> words = articles[i].getWords(); // iau lista de cuvinte fara stopwords
            foreach (String key in words.Keys) // pentru fiecare cuvant
            {

                if (!vocabular.ContainsKey(key))
                {
                    vocabular.Add(key, new List<int>());
                    for (int j = 0; j <= articles.Count; j++)
                        vocabular[key].Add(0);
                    vocabular[key][i] = words[key];
                    
                }
                else

                    vocabular[key][i] = words[key];
                
            }
        }

        foreach (String key in vocabular.Keys) // pentru fiecare cuvant
        {
            int nr = 0;
            for (int z = 0; z < vocabular[key].Count; z++)
                if (vocabular[key][z] != 0)
                    nr++;
            vocabular[key][articles.Count] = nr;
        }
        return vocabular;
    }

    public static List<DocumentBlei> makeBlei(Dictionary<String, List<int>> vocabulary, int year, string conference)
    {
        ArticleHelper articleHelper1 = ArticleHelper.getInstance();
        List<Article> articles = articleHelper1.getAllArticlesByYearAndConference(year, conference);
        List<DocumentBlei> documents = new List<DocumentBlei>();
        int nr;
        Dictionary<int, int> terms;
        for (int i = 0; i < articles.Count; i++)
        {
            nr = 0;
            terms = new Dictionary<int, int>();
            foreach (String word in vocabulary.Keys)
            {
               
               if(vocabulary[word][i]!=0)
                    terms.Add(nr, vocabulary[word][i]);
                nr++;

            }
            documents.Add(new DocumentBlei(nr, terms));
           
        }
        return documents;
    }

    public static Dictionary<int, int>[] convertBleiDictionary(List<DocumentBlei> doc)
    {
        Dictionary<int, int>[] documents = new Dictionary<int, int>[doc.Count];
        for (int i = 0; i < doc.Count; i++)
            documents[i] = doc[i].terms;
        return documents;
    }

    public static Dictionary<int, string> convertBleiVocabulary(Dictionary<String, List<int>> vocabulary)
    {
        Dictionary<int, string> voc = new Dictionary<int, string>();
        int i = 0;
        foreach(String word in vocabulary.Keys)
        {
            voc[i] = word;
            i++;
        }
        return voc;
    }

    public static Dictionary<String, Double> calculateTFIDF(int index, Dictionary<String, List<int>> voc) // for an article calculates the tfidf values for each word and then normalizes it
    {
        Dictionary<String, Double> tfidf_words = new Dictionary<String, Double>();
        Double res;
       
        foreach (String key in voc.Keys) // pentru fiecare cuvant
        {
            res = termFrequency(voc[key][index]) * inverseDocumentFrequency((voc[key].Count)-1,voc[key][voc[key].Count-1]);
            tfidf_words.Add(key, res); // il adaug intr-un dictionar alaturi de tfidf corespunzator
        }
        return normalization(tfidf_words);
    }
}