﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DocumentBlei
/// </summary>
public class DocumentBlei
{
    public int numberOfUniqueTerms { get; set; }
    public Dictionary<int, int> terms { get; set; }
    public DocumentBlei()
    {
        
    }
    public DocumentBlei(int nr, Dictionary<int, int> terms)
    {
        this.numberOfUniqueTerms = nr;
        this.terms = terms;
    }
}