﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Topic
/// </summary>
public class Topic
{

    public int Id, year;
    public String content, conference;

    public Topic(int Id, String content, int year, String conference)
    {
        this.Id = Id;
        this.content = content;
        this.year = year;
        this.conference = conference;
    }

    public Topic(SqlDataReader read)
    {
        this.Id = read.GetInt32(0);
        this.content = read.GetString(1);
        this.year = read.GetInt32(2);
        this.conference = read.GetString(3);
    }
}