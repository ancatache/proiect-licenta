﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Sintagm
/// </summary>
public class Sintagm
{
    public int Id, IdArt, number;
    public Double tfidf;
    public String content;

	public Sintagm(int Id, String content, int number, int IdArt, Double tfidf)
	{
        this.number = number;
        this.content = content;
        this.Id = Id;
        this.IdArt = IdArt;
        this.tfidf = tfidf;
	}

    public Sintagm(SqlDataReader read)
    {
        this.Id = read.GetInt32(0);
        this.content = read.GetString(1);
        this.number = read.GetInt32(2);
        this.IdArt = read.GetInt32(3);
        this.tfidf = read.GetDouble(4);
        Console.WriteLine(this.tfidf);
    }


}