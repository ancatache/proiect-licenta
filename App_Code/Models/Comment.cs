﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Comment
/// </summary>
public class Comment
{
    public int Id, IdArt;
    public String content;

	public Comment(int Id, int IdArt, String content)
	{
        this.Id = Id;
        this.IdArt = IdArt;
        this.content = content;
	}

    public Comment(SqlDataReader read)
    {
        this.Id = read.GetInt32(0);
        this.IdArt = read.GetInt32(1);
        this.content = read.GetString(2);
    }

}