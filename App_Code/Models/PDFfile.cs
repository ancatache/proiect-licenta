﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Web;

/// <summary>
/// Summary description for PDFfile
/// </summary>
public class PDFfile
{ 
    public int Id, IdArt;
    public String Name, Type;

    public PDFfile(int Id, String Name, String Type, int IdArt)
    {
        this.Id = Id;
        this.Name = Name;
        this.Type = Type;
        this.IdArt = IdArt;
       
    }

    public PDFfile(SqlDataReader read)
    {
        this.Id = read.GetInt32(0);
        this.Name = read.GetString(1);
        this.Type = read.GetString(2);
        this.IdArt = read.GetInt32(4);
        
    }
}