﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Category
/// </summary>
public class Category
{
    public int Id;
    public String name;

	public Category(int Id, String name)
	{
        this.Id = Id;
        this.name = name;
	}

    public Category(SqlDataReader read)
    {
        this.Id = read.GetInt32(0);
        this.name = read.GetString(1);
    }
}