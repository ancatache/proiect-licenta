﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Collections;
using MicrosoftResearch.Infer.Distributions;
using edu.stanford.nlp.ie.crf;

/// <summary>
/// Summary description for Article
/// </summary>
public class Article
{
    public int Id, year, published, IdCat;
    public String title, content, description, category, URL, UserName;
   

    public Article(int Id, String title, String content, int year, String description, int published, String category, int IdCat, String URL, String UserName)
    {
        this.Id = Id;
        this.year = year;
        this.published = published;
        this.IdCat = IdCat;
        this.title = title;
        this.content = content;
        this.description = description;
        this.category = category;
        this.URL = URL;
        this.UserName = UserName;
        
    
    }

    public Article(SqlDataReader read)
    {
        this.Id = read.GetInt32(0);
        this.title = read.GetString(1);
        this.content = read.GetString(2);
        this.year = read.GetInt32(3);
        this.description = read.GetString(4);
        this.published = read.GetInt32(5); 
        this.category = read.GetString(6); 
        this.IdCat = read.GetInt32(7); 
        this.URL = read.GetString(8);
        this.UserName = read.GetString(10);
     
    }

/*    private void veziDacaEPrintreCeleMaiMari(KeyValuePair<String, Double>[] lista, KeyValuePair<String, Double> newEntry)
    {
        for (int i = 0; i < 10; i++)
        {
            if (newEntry.Value > lista[i].Value)
            {
                KeyValuePair<String, Double> aux = new KeyValuePair<string, double>(lista[i].Key, lista[i].Value);
                lista[i] = new KeyValuePair<string, double>(newEntry.Key, newEntry.Value);

            }
        }
    }*/
   

    public Sintagm[] getMostPopularThreeSintagms(Dictionary<String, Double> sintagms) // gets the first three sintagms with maximum tfidf value from a dictionary of sintagms
    {
     //   KeyValuePair<String, Double>[] celeMaiMari = new KeyValuePair<String, Double>[10];
        Double max = 0, max1 = 0, max2 = 0;
        String maxKey = " ", maxKey1 = " ", maxKey2 = " ";
        ArticleHelper articleHelper = ArticleHelper.getInstance();
        foreach (String key in sintagms.Keys)
        {
            if (sintagms[key] >= max && key.CompareTo(maxKey)!=0 && key.CompareTo(maxKey1)!=0 && key.CompareTo(maxKey2)!=0)
            {
                max2 = max1;
                max1 = max;
                max = sintagms[key];  
                maxKey2 = maxKey1;
                maxKey1 = maxKey;
                maxKey = key;    
            }
        }
        if(max1 == 0 && max2 == 0)
        {
            foreach (String key in sintagms.Keys)
            {
                if (sintagms[key] >= max1 && key.CompareTo(maxKey) != 0 && key.CompareTo(maxKey1) != 0 && key.CompareTo(maxKey2) != 0)
                {
                    max2 = max1;
                    max1 = sintagms[key];
                    maxKey2 = maxKey1;
                    maxKey1 = key;
                    

                }
            }
        }
        if(max2 == 0)
        {
            foreach (String key in sintagms.Keys)
            {
                if (sintagms[key] >= max2 && key.CompareTo(maxKey) != 0 && key.CompareTo(maxKey1) != 0 && key.CompareTo(maxKey2) != 0)
                {
                    max2 = sintagms[key];
                    maxKey2 = key;                   
                }
            }
        }
        Sintagm[] s = new Sintagm[3];
        s[0] = new Sintagm(-1, maxKey, -1, Id, max);
        s[1] = new Sintagm(-1, maxKey1, -1, Id, max1);
        s[2] = new Sintagm(-1, maxKey2, -1, Id, max2);

        return s;
       
    }


    public Dictionary<String, Double> getTwoWordsSintagms(Dictionary<String, Double> tfidfWords) // gets all sintagms composed by two words from current article and calculate for each sintagm its tfidf value
    {
        Dictionary<String, Double> sintagms = new Dictionary<String, Double>();
        String cword=null;
        StringBuilder builder = new StringBuilder();
        Dictionary<String, bool> stopWords = WordUtils.getStopWords();
        content = WordUtils.removeSpecialCharacters(content);
        String[] words = WordUtils.splitText(content);       
        for (int i = 0; i < words.Length ; i++)
        {
            if (!stopWords.ContainsKey(words[i].ToLower()) && words[i].Length > 3 && WordUtils.isFromCharacters(words[i].ToLower()))
            {
                if (cword != null && cword.CompareTo(words[i].ToLower()) != 0)
                {
                    String twoWords = cword + " " + words[i].ToLower();
                    Double tfidfSintagm = (tfidfWords[cword] * tfidfWords[words[i].ToLower()]) / 2;
                    cword = words[i].ToLower();
                    builder.Append(cword).Append(' ');
                    if (!sintagms.ContainsKey(twoWords))
                    {
                        
                        sintagms.Add(twoWords, tfidfSintagm);
                    }
                   
                }
                else cword = words[i].ToLower();

            }
            else
            {
                cword = null;
            }
        }

        return sintagms;        
    }

 /*   public List<Sintagm> getTwoWordsSintagms1(int id) // gets all sintagms composed by two words from current article and calculate for each sintagm its tfidf value
    {
        Dictionary<String, int> sintagms = new Dictionary<String, int>();
      //  Dictionary<String, Double> tfidfWords = this.calculateTFIDF();
        
        String cword = null;
        StringBuilder builder = new StringBuilder();
        Dictionary<String, bool> stopWords = WordUtils.getStopWords();
        content = WordUtils.removeSpecialCharacters(content);
        String[] words = WordUtils.splitText(content);
        for (int i = 0; i < words.Length; i++)
        {
            if (!stopWords.ContainsKey(words[i].ToLower()))
            {
                if (cword != null)
                {
                    String twoWords = cword + " " + words[i].ToLower();
                 //   Double tfidfSintagm = (tfidfWords[cword] * tfidfWords[words[i].ToLower()]) / 2;
                    cword = words[i].ToLower();
                    builder.Append(cword).Append(' ');
                    if (!sintagms.ContainsKey(twoWords))
                    {

                        sintagms.Add(twoWords, 1);
                    }
                    else
                        sintagms[twoWords]++;

                }
                else cword = words[i].ToLower();

            }
        }
        List<Sintagm> sin = new List<Sintagm>();
        foreach (String key in sintagms.Keys)
        {
            if (sintagms[key] > 2)
            {
               // String[] s = WordUtils.splitSintagm(key);

              //  Double tfidfSintagm = (tfidfWords[s[0]] * tfidfWords[s[1]]) / 2;
                sin.Add(new Sintagm(-1, key, sintagms[key], id, -1));
            }
        }


        return sin;
    }*/



    public Dictionary<String, Double> getThreeWordsSintagms( Dictionary<String, Double> tfidfWords) // gets all sintagms composed by three words from current article and calculate for each sintagm its tfidf value
    {
        Dictionary<String, Double> sintagms = new Dictionary<String, Double>();
        String cword = null;
        String sword = null;
        StringBuilder builder = new StringBuilder();
        Dictionary<String, bool> stopWords = WordUtils.getStopWords();
        content = WordUtils.removeSpecialCharacters(content);
        String[] words = WordUtils.splitText(content);
        for (int i = 0; i < words.Length; i++)
        {
            
                if (!stopWords.ContainsKey(words[i].ToLower()) && words[i].Length > 3 && WordUtils.isFromCharacters(words[i].ToLower()))
                {

                    if (cword != null && sword != null && sword.CompareTo(words[i].ToLower()) != 0)
                    {
                        String threeWords = cword + " " + sword + " " + words[i].ToLower();
                        Double tfidfSintagm = (tfidfWords[cword] * tfidfWords[sword] * tfidfWords[words[i].ToLower()]) / 3;
                        cword = sword;
                        builder.Append(cword).Append(' ');
                        sword = words[i].ToLower();
                        builder.Append(sword).Append(' ');
                        if (!sintagms.ContainsKey(threeWords))
                        {

                            sintagms.Add(threeWords, tfidfSintagm);
                        }


                    }
                    else
                        if (cword != null && sword == null && cword.CompareTo(words[i].ToLower()) != 0)
                    {
                        sword = words[i].ToLower();
                    }
                    else
                        cword = words[i].ToLower();

                }
                else
                {
                    cword = null;
                    sword = null;
                }
           
        }



        return sintagms;
    }

 /*   public List<Sintagm> getThreeWordsSintagms1(int id) // gets all sintagms composed by three words from current article and calculate for each sintagm its tfidf value
    {
        Dictionary<String, int> sintagms = new Dictionary<String, int>();
      //  Dictionary<String, Double> tfidfWords = this.calculateTFIDF();
        String cword = null;
        String sword = null;
        StringBuilder builder = new StringBuilder();
        Dictionary<String, bool> stopWords = WordUtils.getStopWords();
        content = WordUtils.removeSpecialCharacters(content);
        String[] words = WordUtils.splitText(content);
        for (int i = 0; i < words.Length; i++)
        {
            if (!stopWords.ContainsKey(words[i].ToLower()))
            {

                if (cword != null && sword != null)
                {
                    String threeWords = cword + " " + sword + " " + words[i].ToLower();
                 //   Double tfidfSintagm = (tfidfWords[cword] * tfidfWords[sword] * tfidfWords[words[i].ToLower()]) / 3;
                    cword = sword;
                    builder.Append(cword).Append(' ');
                    sword = words[i].ToLower();
                    builder.Append(sword).Append(' ');
                    if (!sintagms.ContainsKey(threeWords))
                    {

                        sintagms.Add(threeWords, 1);
                    }
                    else
                        sintagms[threeWords]++;

                }
                else
                    if (cword != null && sword == null)
                    {
                        sword = words[i].ToLower();
                    }
                    else
                        cword = words[i].ToLower();

            }
        }

        List<Sintagm> sin = new List<Sintagm>();
        foreach(String key in sintagms.Keys)
        {
            if(sintagms[key]>2)
                sin.Add(new Sintagm(-1, key, sintagms[key], id, -1));
        }


        return sin;
    }*/
    
    public Boolean containsSintagm(String sintagm)
    {
        
        if (content.ToLower().Contains(sintagm))
            return true;
        else
            return false;
    }

    public Boolean containsWord(String word)
    {
        int ok = 0;
        Dictionary<String, int> wordsDictionary = getWords();
        for (int i = 0; i < wordsDictionary.Count; i++)
            if (wordsDictionary.ContainsKey(word.ToLower()))
                ok++;
        if (ok != 0)
            return true;
        else
            return false;            
    }

    public int numberOfAppearance(String word)
    {
        int nr=0;
        String[] words = WordUtils.splitText(content);
        for(int i=0; i<words.Length;i++)
            if(words[i].ToLower().Equals(word))
                nr++;
        return nr;
    }

    public Dictionary<String, int> getWords()
    {
        Dictionary<String, int> wordsDictionary = new Dictionary<String, int>();
        Dictionary<String, bool> stopWords = WordUtils.getStopWords();
        content = WordUtils.removeSpecialCharacters(content);
        String[] words = WordUtils.splitText(content);
        for (int i = 0; i < words.Length; i++)
        {
            if (!stopWords.ContainsKey(words[i].ToLower()))
            {
                if (!wordsDictionary.ContainsKey(words[i].ToLower()))
                    wordsDictionary.Add(words[i].ToLower(), 1);
                else
                    wordsDictionary[words[i].ToLower()]++;
            }
        }
        return wordsDictionary;

    }

    public Word getMostPopularWord(Dictionary<String, int> words)
    {
        int max = 0;
        String maxKey = " ";
        foreach (String key in words.Keys)
        {
            if (words[key] > max)
            {
                max = words[key];
                maxKey = key;
            }
        }
        return new Word(maxKey, max);
    }

  /*  public double tfidf(List<Article> articles, String word)
    {
        double first = WordUtils.termFrequency(word, this);
        double second = WordUtils.inverseDocumentFrequency(word, articles);
       return first * second;
    }*/

   

   

   /* public Dictionary<String, Double> calculateTFIDF() // for an article calculates the tfidf values for each word and then normalizes it
    {
        Dictionary<String, int> words = this.getWords(); // iau lista de cuvinte fara stopwords
        ArticleHelper articleHelper = ArticleHelper.getInstance();
        List<Article> articles = articleHelper.getAllArticles(); // iau toate articolele
        Dictionary<String, Double> tfidf_words = new Dictionary<String, Double>();
        Double res;
        foreach (String key in words.Keys) // pentru fiecare cuvant
        {

            res = this.tfidf(articles, key); // calculez tfidf
            tfidf_words.Add(key, res); // il adaug intr-un dictionar alaturi de tfidf corespunzator
        }
        return WordUtils.normalization(tfidf_words);
    }*/
}