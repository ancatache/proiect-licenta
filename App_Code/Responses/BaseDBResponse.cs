﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BaseDBResponse
/// </summary>
public class BaseDBResponse
{
    public String message;
    public int id;
	public BaseDBResponse(String message, int id)
	{
        this.message = message;
        this.id = id;
	}
}