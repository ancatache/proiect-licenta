﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Word
/// </summary>
public class Word
{
    public String content;
    public Double tfidf;
	public Word(String content, Double tfidf)
	{
        this.content = content;
        this.tfidf = tfidf;
	}
}