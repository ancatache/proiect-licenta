﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CategoryHelper
/// </summary>
public class CategoryHelper
{
    private static CategoryHelper categoryHelper;
    private const String connectionStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\Articles.mdf;Integrated Security=True";

	private CategoryHelper()
	{
        
	}

    public static CategoryHelper getInstance()
    {
        if (categoryHelper == null)
            categoryHelper = new CategoryHelper();
        return categoryHelper;
    }

    public BaseDBResponse addCategory(Category category)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "INSERT INTO Category (name) OUTPUT INSERTED.ID VALUES (@name)";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("name", category.name);
            Int32 id = (Int32)com.ExecuteScalar();
            return new BaseDBResponse("New category added successfully!", id);   
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    public Category getCategoryByName(String name)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "SELECT * from Category where name=@name";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("name", name);
            com.ExecuteNonQuery();
            var read = com.ExecuteReader();
            if (!read.HasRows)
            {
                return null;
            }
            else
            {
                read.Read();
                return new Category(read);
            }
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public void deleteAllCategories()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "DELETE FROM [Category]";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.ExecuteNonQuery();
        }
        catch (SqlException se)
        {
            String s = se.Message;
            Console.WriteLine(s);

        }
        catch (Exception e)
        {
            String s = e.Message;
            Console.WriteLine(s);

        }
        finally
        {
            con.Close();
        }
    }

}