﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Summary description for PDFfileHelper
/// </summary>
public class PDFfileHelper
{
    private static PDFfileHelper pdfFileHelper;
    private const String connectionStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\Articles.mdf;Integrated Security=True";

	private PDFfileHelper()
	{
		
	}

    public static PDFfileHelper getInstance()
    {
        if (pdfFileHelper == null)
            pdfFileHelper = new PDFfileHelper();
        return pdfFileHelper;
    }

    public BaseDBResponse savePDF(PDFfile pdfFile)
    {
        if (findPDFById(pdfFile.Id) != null)
            return updatePDF(pdfFile);
        else
            return addPDF(pdfFile);
    }

    private BaseDBResponse addPDF(PDFfile pdfFile)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "INSERT INTO PDFfiles (Name, Type, IdArt) OUTPUT INSERTED.ID"
                    + " VALUES (@Name, @Type, @IdArt)";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Name", pdfFile.Name);
            com.Parameters.AddWithValue("Type", pdfFile.Type);
            com.Parameters.AddWithValue("IdArt", pdfFile.IdArt);
        
            Int32 id = (Int32)com.ExecuteScalar();
            return new BaseDBResponse("New PDF added successfully!", id);
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    private BaseDBResponse updatePDF(PDFfile pdfFile)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "UPDATE PDFfiles SET Name = @Name, Type = @Type, IdArt=@IdArt"
                    + " WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", pdfFile.Id);
            com.Parameters.AddWithValue("Name", pdfFile.Name);
            com.Parameters.AddWithValue("Type", pdfFile.Type);
            com.Parameters.AddWithValue("IdArt", pdfFile.IdArt);
           
            com.ExecuteNonQuery();
            return new BaseDBResponse("PDFfile updated successfully!", pdfFile.Id);
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    public String deletePDFByIdArt(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "DELETE FROM [PDFfiles] WHERE IdArt=@Id ";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            com.ExecuteNonQuery();
            return "";
        }
        catch (Exception e)
        {
            return e.Message;
        }
        finally
        {
            con.Close();
        }
    }

    public Article findPDFById(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "Select * from PDFfiles WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            int d = com.ExecuteNonQuery();
            var read = com.ExecuteReader();
            if (!read.HasRows)
            {
                return null;
            }
            else
            {
                read.Read();
                return new Article(read);
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public DataSet getPdfByIdArt(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "Select * from PDFfiles WHERE IdArt=@Id";
        con.Open();
        try
        {
            SqlDataAdapter adapter = new SqlDataAdapter(query, con);
            DataSet dataSet = new DataSet();
            adapter.Fill(dataSet);
            return dataSet;
        }
          catch (Exception ex)
        {
            Console.Write(ex.Message);
            return null;
            
        }
        finally
        {
            con.Close();
        }
       
    }

}