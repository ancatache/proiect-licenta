﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ArticleHelper
/// </summary>
public class ArticleHelper
{
    private static ArticleHelper articleHelper;

    
    private const String connectionStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\Articles.mdf;Integrated Security=True";

	private ArticleHelper()
	{
        
	}

    public static ArticleHelper getInstance()
    {
        if (articleHelper == null)
            articleHelper = new ArticleHelper();
        return articleHelper;     
    }

    public BaseDBResponse saveArticle(Article article)
    {
        if (findArticleById(article.Id) != null)
            return updateArticle(article);
        else
            return addArticle(article);
    }

    private BaseDBResponse addArticle(Article article)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "INSERT INTO Article (title, content, year, description, published, category, IdCat, URL, UserName) OUTPUT INSERTED.ID"
                    + " VALUES (@title, @content, @year, @description, @published, @category, @IdCat, @URL, @UserName)";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("title", article.title);
            com.Parameters.AddWithValue("content", article.content);
            com.Parameters.AddWithValue("year", article.year);
            com.Parameters.AddWithValue("description", article.description);
            com.Parameters.AddWithValue("published", article.published);
            com.Parameters.AddWithValue("category", article.category);
            com.Parameters.AddWithValue("IdCat", article.IdCat);
            com.Parameters.AddWithValue("URL", article.URL);
            com.Parameters.AddWithValue("UserName", article.UserName);
            Int32 id = (Int32)com.ExecuteScalar();
            return new BaseDBResponse("New article added successfully!", id);            
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }        
        finally
        {
            con.Close();
        }
    }

    private BaseDBResponse updateArticle(Article article)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "UPDATE Article SET title = @title, content = @content, year = @year, description = @description, published = @published, category = @category, IdCat = @IdCat, URL = @URL, UserName = @UserName"
                    + " WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", article.Id);
            com.Parameters.AddWithValue("title", article.title);
            com.Parameters.AddWithValue("content", article.content);
            com.Parameters.AddWithValue("year", article.year);
            com.Parameters.AddWithValue("description", article.description);
            com.Parameters.AddWithValue("published", article.published);
            com.Parameters.AddWithValue("category", article.category);
            com.Parameters.AddWithValue("IdCat", article.IdCat);
            com.Parameters.AddWithValue("URL", article.URL);
            com.Parameters.AddWithValue("UserName", article.UserName);
            com.ExecuteNonQuery();
            return new BaseDBResponse("Article updated successfully!", article.Id);
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }


    public String addImage(Int32 id, FileUpload fileUpload, HttpServerUtility server)
    {
        try
        {
            if (fileUpload.HasFile)
            {
                if (fileUpload.PostedFile.ContentType.ToLower().EndsWith("jpeg"))
                    fileUpload.SaveAs(server.MapPath("~") + "/Images/" + id + ".jpg");
                else
                    return "Image file is not in JPEG format! Format is: " + fileUpload.PostedFile.ContentType.ToUpper();
            }
            else
                return "No image file uploaded!";
            return "New article added successfully!";        
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public String addPDF(Int32 id, FileUpload fileUpload, HttpServerUtility server)
    {
        try
        {
            if (fileUpload.HasFile)
            {
                if (fileUpload.PostedFile.ContentType.ToLower().EndsWith("pdf"))
                
                    fileUpload.SaveAs(server.MapPath("~") + "/PDFs/" + id + ".pdf");
       
                else
                    return "File is not in JPDF format! Format is: " + fileUpload.PostedFile.ContentType.ToUpper();
            }
            else
                return "No pdf file uploaded!";
            return "New article added successfully!";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public Article findArticleById(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "Select * from Article WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            int d = com.ExecuteNonQuery();
            var read = com.ExecuteReader();
            if (!read.HasRows)
            {
                return null;
            }
            else
            {
                read.Read();
                return new Article(read);           
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public String deleteArticle(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String quer1 = "DELETE FROM [SINTAGM] WHERE IdArt=@Id";
        String query = "DELETE FROM [ARTICLE] WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com1 = new SqlCommand(quer1,con);
            SqlCommand com = new SqlCommand(query, con);
            com1.Parameters.AddWithValue("Id", Id);
            com.Parameters.AddWithValue("Id", Id);
            com1.ExecuteNonQuery();
            com.ExecuteNonQuery();
            return "";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
        finally
        {
            con.Close();
        }
    }

    public Article getArticleById(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "SELECT * FROM Article where Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            var read = com.ExecuteReader();
            if (read.HasRows)
            {
                read.Read();
                return new Article(read);
            }
            else
                return null;                                                  
        }
        catch(Exception e)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Article> getAllArticles()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Article> articles = new List<Article>();
        String query = "SELECT * FROM [Article] order by Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    articles.Add(new Article(read));
            else
                articles = null;
            return articles;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public void setUserNameFields()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "UPDATE Article SET UserName = @UserName";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("UserName", "");
            com.ExecuteNonQuery();

        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            
        }
        finally
        {
            con.Close();
        }
    }

    public List<Article> getAllArticlesByUserName(String UserName)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Article> articles = new List<Article>();
        String query = "SELECT * FROM [Article] where UserName = @UserName";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("UserName", UserName);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    articles.Add(new Article(read));
            else
                articles = null;
            return articles;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }


    public List<Article> getAllArticlesByYear(int year)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Article> articles = new List<Article>();
        String query = "Select * from [Article] where year=@year";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("year", year);
            var read = com.ExecuteReader();
            if (read.HasRows)
            {
                while (read.Read())
                    articles.Add(new Article(read));
                return articles;
            }
            else
                return null;
        }
        catch (Exception e)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Article> getAllArticlesByYearAndConference(int year, string conference)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Article> articles = new List<Article>();
        String query = "Select * from [Article] where year=@year and category=@conference";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("year", year);
            com.Parameters.AddWithValue("conference", conference);
            var read = com.ExecuteReader();
            if (read.HasRows)
            {
                while (read.Read())
                    articles.Add(new Article(read));
                return articles;
            }
            else
                return null;
        }
        catch (Exception e)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Article> getAllArticlesByConference(string conference)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Article> articles = new List<Article>();
        String query = "Select * from [Article] where category=@category";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("category", conference);
            var read = com.ExecuteReader();
            if (read.HasRows)
            {
                while (read.Read())
                    articles.Add(new Article(read));
                return articles;
            }
            else
                return null;
        }
        catch (Exception e)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
    }


}