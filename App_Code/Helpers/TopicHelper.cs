﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TopicHelper
/// </summary>
public class TopicHelper
{
    private static TopicHelper topicHelper;

   
    private const String connectionStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\Articles.mdf;Integrated Security=True";

    private TopicHelper()
    {
        
    }

    public static TopicHelper getInstance()
    {
        if (topicHelper == null)
            topicHelper = new TopicHelper();
        return topicHelper;
    }


    public BaseDBResponse saveTopic(Topic topic)
    {
        if (findTopicById(topic.Id) != null)
            return updateTopic(topic);
        else
            return addTopic(topic);
    }

    private BaseDBResponse addTopic(Topic topic)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "INSERT INTO Topic (content, year, conference) OUTPUT INSERTED.ID"
                    + " VALUES (@content, @year, @conference)";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("content", topic.content);
            com.Parameters.AddWithValue("year", topic.year);
            com.Parameters.AddWithValue("conference", topic.conference);
            Int32 id = (Int32)com.ExecuteScalar();
            return new BaseDBResponse("New topic added successfully!", id);
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    private BaseDBResponse updateTopic(Topic topic)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "UPDATE Topic SET content = @content, year=@year, conference = @conference"
                    + " WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id",topic.Id);
            com.Parameters.AddWithValue("content", topic.content);
            com.Parameters.AddWithValue("year", topic.year);
            com.Parameters.AddWithValue("conference", topic.conference);
            com.ExecuteNonQuery();
            return new BaseDBResponse("Topic updated successfully!", topic.Id);
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    public Topic findTopicById(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "Select * from Topic WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            int d = com.ExecuteNonQuery();
            var read = com.ExecuteReader();
            if (!read.HasRows)
            {
                return null;
            }
            else
            {
                read.Read();
                return new Topic(read);
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> getAllTopics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic]";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> get2008Topics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic] WHERE YEAR = 2008";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> get2012Topics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic] WHERE YEAR = 2012";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> get2016Topics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic] WHERE YEAR = 2016";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> getNIPSTopics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic] WHERE CONFERENCE = @NIPS";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("NIPS", "NIPS");
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> getACLTopics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic] WHERE CONFERENCE = @ACL";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("ACL", "ACL");
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Topic> getCVPRTopics()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Topic> topics = new List<Topic>();
        String query = "SELECT * FROM [Topic] WHERE CONFERENCE = @CVPR";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("CVPR", "CVPR");
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    topics.Add(new Topic(read));
            else
                topics = null;
            return topics;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }
}