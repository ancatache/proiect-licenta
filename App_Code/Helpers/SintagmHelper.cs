﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Sintagm
/// </summary>
public class SintagmHelper
{
    private static SintagmHelper sintagmHelper;
    private const String connectionStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\Articles.mdf;Integrated Security=True";

	private SintagmHelper()
	{
        
	}

    public static SintagmHelper getInstance()
    {
        if (sintagmHelper == null)
            return new SintagmHelper();
        else
            return sintagmHelper;
    }

    public BaseDBResponse saveSintagm(Sintagm sintagm)
    {
        if (findSintagmById(sintagm.Id) != null)
            return updateSintagm(sintagm);
        else
            return addSintagm(sintagm);
    }

    public BaseDBResponse addSintagm(Sintagm sintagm)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        string query = "Insert into Sintagm(content,number,IdArt,tfidf) OUTPUT INSERTED.ID VALUES(@content,@number,@IdArt,@tfidf)";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("content", sintagm.content);
            com.Parameters.AddWithValue("number", sintagm.number);
            com.Parameters.AddWithValue("IdArt", sintagm.IdArt);
            com.Parameters.AddWithValue("tfidf", sintagm.tfidf);
            Int32 id = (Int32)com.ExecuteScalar();
            return new BaseDBResponse("New sintagm added successfully!", id); 
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    private BaseDBResponse updateSintagm(Sintagm sintagm)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        string query = "UPDATE Sintagm SET content=@content,number=@number,IdArt=@IdArt,tfidf=@tfidf WHERE Id=@Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("content", sintagm.content);
            com.Parameters.AddWithValue("number", sintagm.number);
            com.Parameters.AddWithValue("IdArt", sintagm.IdArt);
            com.Parameters.AddWithValue("tfidf", sintagm.tfidf);
            com.Parameters.AddWithValue("Id", sintagm.Id);
            com.ExecuteNonQuery();
            return new BaseDBResponse("New sintagm added successfully!", sintagm.Id); 
        }
        catch (SqlException se)
        {
            return new BaseDBResponse(se.Message, -1);
        }
        catch (Exception ex)
        {
            return new BaseDBResponse(ex.Message, -1);
        }
        finally
        {
            con.Close();
        }
    }

    public Sintagm findSintagmById(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "Select * from Sintagm WHERE Id=@Id";
  
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            int d = com.ExecuteNonQuery();
            var read = com.ExecuteReader();
            if (!read.HasRows)
            {
                return null;
            }
            else
            {
                while (read.Read())
                   return new Sintagm(read);
               return null;
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Sintagm> findSintagmByIdArt(int IdArt)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "Select * from Sintagm WHERE IdArt=@IdArt";
        List<Sintagm> sin = new List<Sintagm>();
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("IdArt", IdArt);
            int d = com.ExecuteNonQuery();
            var read = com.ExecuteReader();
            if (!read.HasRows)
            {
                return new List<Sintagm>();
            }
            else
            {
                while(read.Read())
                    sin.Add(new Sintagm(read));
                return sin;
            }
        }
        catch (Exception ex)
        {
            Console.Write(ex.Message);
            return new List<Sintagm>();
        }
        finally
        {
            con.Close();
        }
    }

    public List<Sintagm> getAllSintagmsOrdered()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        List<Sintagm> sintagms = new List<Sintagm>();
        String query = "SELECT * FROM Sintagm order by tfidf desc";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())
                    sintagms.Add(new Sintagm(read));
            else
                sintagms = null;
            return sintagms;
        }
        catch (SqlException se)
        {
            String s = se.Message;
            Console.WriteLine(s);
            return null;
        }
        catch (Exception e)
        {
            String s = e.Message;
            Console.WriteLine(s);
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public void deleteSintagmByIdArticle(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "DELETE FROM [Sintagm] WHERE IdArt = @Id";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            com.ExecuteNonQuery();
        }
        catch (SqlException se)
        {
            String s = se.Message;
            Console.WriteLine(s);
            
        }
        catch (Exception e)
        {
            String s = e.Message;
            Console.WriteLine(s);
            
        }
        finally
        {
            con.Close();
        }
    }


    public void deleteAllSintagms()
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "DELETE FROM [Sintagm]";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.ExecuteNonQuery();
        }
        catch (SqlException se)
        {
            String s = se.Message;
            Console.WriteLine(s);

        }
        catch (Exception e)
        {
            String s = e.Message;
            Console.WriteLine(s);

        }
        finally
        {
            con.Close();
        }
    }

}