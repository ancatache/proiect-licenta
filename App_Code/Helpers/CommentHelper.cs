﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CommentHelper
/// </summary>
public class CommentHelper
{
    static CommentHelper commentHelper;
    private const String connectionStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\Articles.mdf;Integrated Security=True";

	private CommentHelper()
	{
        
	}

    static public CommentHelper getInstance()
    {
        if (commentHelper == null)
            commentHelper = new CommentHelper();
        return commentHelper;
    }

    public String addComment(Comment comment)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "INSERT INTO [Table] (IdArt, content)"
                        + " VALUES (@idart, @content)";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("IdArt", comment.IdArt);
            com.Parameters.AddWithValue("content", comment.content);
            com.ExecuteNonQuery();
            return "";
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
        finally
        {
            con.Close();
        }
    }

    public List<Comment> getAllCommentsByIdArt(int IdArt)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "SELECT * FROM [Table] where IdArt=@IdArt";
        List<Comment> Comments = new List<Comment>();
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("IdArt", IdArt);
            var read = com.ExecuteReader();
            if (read.HasRows)
                while (read.Read())            
                    Comments.Add(new Comment(read));
            return Comments;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            con.Close();
        }
    }

    public String deleteAllCommentsByIdArt(int Id)
    {
        SqlConnection con = new SqlConnection(connectionStr);
        String query = "DELETE FROM [TABLE] WHERE IdArt=@Id ";
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(query, con);
            com.Parameters.AddWithValue("Id", Id);
            com.ExecuteNonQuery();
            return "";
        }
        catch(Exception e)
        {
            return e.Message;
        }
        finally
        {
            con.Close();
        }
    }
}