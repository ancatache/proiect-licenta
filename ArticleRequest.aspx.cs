﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ArticleRequest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void add_art(object sender, EventArgs e)
    {
        Repeater repeter = LVProfile.FindControl("Repeater1") as Repeater;
        Literal literal = LVProfile.FindControl("LAnswer") as Literal;

        if (repeter != null)
        {
            foreach (RepeaterItem item in repeter.Items)
            {
                HyperLink HP = (HyperLink)item.FindControl("HyperLink3");
                String Id1 = HP.NavigateUrl;
                int index = Id1.IndexOf('?');
                int Id = Int32.Parse(Id1.Substring(index + 1, Id1.Length - index - 1));
                CheckBox cb = (CheckBox)item.FindControl("CheckBox1");
                if (cb.Checked)
                {
                    ArticleHelper articleHelper = ArticleHelper.getInstance();
                    Article article = articleHelper.findArticleById(Id);
                    if (article != null)
                    {
                        article.published = 1;
                        BaseDBResponse baseDBResponse = articleHelper.saveArticle(article);
                        if (baseDBResponse.id == -1 && literal!=null)
                            literal.Text = baseDBResponse.message;
                        else
                            Response.Redirect("Home.aspx", false);
                    }
                }
            }
        }
    }
}