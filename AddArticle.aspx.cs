﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class AddArticle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { 
            DropDownList ddl = LVProfile.FindControl("DDLYear") as DropDownList;
            if(ddl!=null)
                ddl.DataBind();
        }
    }

    public string[] Last100Years
    {
        get
        {
            DateTime now = DateTime.Now;
            string[] years = new string[100];

            for (int i = 0; i < years.Length; i++)
            {
                years[i] = (now.Year - i).ToString();
            }
            return years;
        }
    }

    protected void AddArticle_Button(object sender, EventArgs e)
    {
        
        if (Page.IsValid)
        {

            TextBox conference = LVProfile.FindControl("Conference") as TextBox;
            DropDownList ddl = LVProfile.FindControl("DDLYear") as DropDownList;
            FileUpload file = LVProfile.FindControl("FileUpload") as FileUpload;
            Literal answer = LVProfile.FindControl("LAnswer") as Literal;
            int IdCat=0;
            String result;
            string UserName = Page.User.Identity.Name;               
            string role="";
            int published = 0;
            string nameCategory = "";
            if (conference!=null)
                nameCategory = conference.Text.ToString(); 
            string title = "";
            string content ="";
            string description="";
            string URL = "";
            int year=0;
            if (ddl!=null)
                year = int.Parse(ddl.SelectedValue);

              
            string filePath = Server.MapPath(file.PostedFile.FileName);// getting the file path of uploaded file
            string fileName = Path.GetFileName(filePath);               // getting the file name of uploaded file
            string ext = Path.GetExtension(fileName);                      // getting the file extension of uploaded file
            string type = String.Empty;
            Stream fs = file.PostedFile.InputStream;


            if (file.HasFile)
            {
                try
                {

                    switch (ext)                                         // this switch code validate the files which allow to upload only PDF  file 
                    {
                        case ".pdf":
                            type = "application/pdf";
                            break;

                    }

                    if (type != String.Empty)
                    {
                                  
                                result = PDFParser.ExtractTextFromPdf(filePath);
                                content = result;
                                title = fileName;
                    }

                }

                catch (Exception ex)
                {
                    String err = ex.Message.ToString();
                   //   Label2.Text = "Error: " + ex.Message.ToString();
                }


            }
       
                
            // content = result;
            if (content.Length > 1000)
                description = content.Substring(0, 1000);
            else
                description = content;
                
            string query = "SELECT UsersInRoles.RoleId FROM UsersInRoles INNER JOIN Users ON UsersInRoles.UserId = Users.UserId where UserName= @UserName";

            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Users\Sebi\Desktop\anca\Proiect\App_Data\AspnetDB.mdf;Integrated Security=True");
            con.Open();
            try
            {
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("UserName", UserName);
                int d = com.ExecuteNonQuery();
                var read = com.ExecuteReader();
                if (read.HasRows)
                {
                    while (read.Read())
                    {

                        if (read.GetGuid(0).ToString().Equals("ff377b68-db32-4094-b666-e4db6b426476"))
                            role = "user";
                        else
                            if (read.GetGuid(0).ToString().Equals("323d5c51-d5d5-4767-bb26-5f07a360eb98"))
                                role = "admin";
                            else
                                if (read.GetGuid(0).ToString().Equals("5da21c5a-f1be-4cf9-9aa3-c640b8415806"))
                                        role = "editor";

                    }
                }
                read.Close();
            }
            catch (Exception ex)
            {

            answer.Text = "Database insert error : " + ex.Message;
            }
               
            if(role.Equals("editor"))
                published = 1;
            else
                if(role.Equals("user"))
                    published = 0;

            CategoryHelper categoryHelper = CategoryHelper.getInstance();
            Category category = categoryHelper.getCategoryByName(nameCategory);
            if (category == null)
            {
                category = new Category(-1, nameCategory);
                BaseDBResponse response = categoryHelper.addCategory(category);
                if (response.id == -1)
                answer.Text = response.message;
                else
                    IdCat = response.id;
            }
            else
                IdCat = category.Id;
                
            Article article = new Article(-1, title, content, year, description, published, nameCategory, IdCat, URL, UserName);
            ArticleHelper articleHelper = ArticleHelper.getInstance();
            BaseDBResponse baseDBResponse = articleHelper.saveArticle(article);
            if (baseDBResponse.id != -1)
            {
            //    LAnswer.Text = articleHelper.addImage(baseDBResponse.id, FUImage, Server);
                answer.Text = articleHelper.addPDF(baseDBResponse.id, file, Server);
                PDFfile pdfFile = new PDFfile(-1, fileName, type, baseDBResponse.id);
                PDFfileHelper pdfHelper = PDFfileHelper.getInstance();
                BaseDBResponse baseDBResponsePdf = pdfHelper.savePDF(pdfFile);
                //  DDLYear.SelectedIndex = 0;
                // Conference.Text = "";
                // Title.Text = "";
                // TextArea1.Value = "";
                //  TextBoxC.Text = "";
                //  TextBoxURL.Text = "";
                conference.Text = " ";
                ddl.SelectedIndex = 0;
                



            }
            else
            answer.Text = baseDBResponse.message;

            BaseDBResponse b;
               
            //   Dictionary<String, Double> tfidfWords = article.calculateTFIDF();
                

            /* SintagmHelper sintagmHelper = SintagmHelper.getInstance();
            sintagmHelper.deleteAllSintagms();
            ArticleHelper articleHelper1 = ArticleHelper.getInstance();
            List<Article> articles = articleHelper1.getAllArticles();
            for (int i = 0; i < articles.Count; i++)
            {
                Dictionary<String, Double> twoWordsSintagms = articles[i].getTwoWordsSintagms();
                Dictionary<String, Double> threeWordsSintagms = articles[i].getThreeWordsSintagms();
                Dictionary<String, Double> oneSingleDictionary = new Dictionary<String, Double>(); // combines the two dictionaries of two respective three words 
                twoWordsSintagms.ToList().ForEach(x => oneSingleDictionary.Add(x.Key, x.Value));
                threeWordsSintagms.ToList().ForEach(x => oneSingleDictionary.Add(x.Key, x.Value));
                Sintagm[] s = articles[i].getMostPopularThreeSintagms(oneSingleDictionary);
                   
                BaseDBResponse response;
                for (int j = 0; j < s.Length; j++)
                    response = sintagmHelper.saveSintagm(s[j]);
            }*/
                

               
       
             
    }

       
        


    }

   
}
       