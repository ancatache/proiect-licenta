﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManageRoles.aspx.cs" Inherits="ManageRoles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:LoginView ID="LVProfile" runat="server">

        <AnonymousTemplate>
            <div style="background-image: url('bc3.png'); background-attachment: fixed; height: 1000px; width: 100%;">
            <div style="padding-top:350px">
                <div  class="addArticle" style="padding:50px">
                    This page has restricted access.
                    Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink> or sign in.
                </div>
            </div>
            </div>
        </AnonymousTemplate>


        <RoleGroups>
            <asp:RoleGroup Roles="Admin">
                <ContentTemplate>
                    <div style="background-image: url('bc3.png'); background-attachment: fixed; height: auto; width: 100%;">
                    <div style="padding-top:350px; padding-bottom:100px;">
                        <div style="margin-left:500px;">
                            <asp:Literal ID="LAnswer" runat="server"></asp:Literal>
                        </div>
                        <br />
                        <br />
                        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
                            <ItemTemplate>
                                <div class="articleContainer" style="width: 50%; margin-left:150px; padding:30px;  background: rgba(255, 255, 255, 0.7); box-shadow: 2px 2px 5px #888888;" id="showArticles" runat="server">
                                    <asp:Label ID="Label1" runat="server" Text="Username:"></asp:Label>
                                    <asp:Label style="color:#F9766E" ID="UserLabel" runat="server" Text='<%# Eval("UserName") %>'></asp:Label>
                                    <br /><br />
                                    <asp:Label ID="Label2" runat="server" Text="Role:"></asp:Label>
                                    <asp:DropDownList ID="DropDownList1" runat="server">
                                        <asp:ListItem Value="User"> User </asp:ListItem>
                                        <asp:ListItem Value="Editor"> Editor </asp:ListItem>
                                        <asp:ListItem Value="Admin"> Admin </asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <br />
           
                                    <asp:Label style="visibility:hidden" ID="LabelId" runat="server" Text='<%# Eval("UserId") %>'></asp:Label>
                                    <br />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <br />
   
                        <div style="padding-left:500px">
            
                            <asp:Button ID="Button1" runat="server" Text="Save" OnClick="SaveChanges" CssClass="redButton"  />
                        </div>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>

        <LoggedInTemplate>
            <div style="background-image: url('bc3.png'); background-attachment: fixed; height: 1000px; width: 100%;">
                <div style="padding-top:350px">
                    <div  class="addArticle" style="padding:50px">
                        This page has restricted access.
                        Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink> or sign in as admin.
                    </div>
                </div>
            </div>
        </LoggedInTemplate>

    </asp:LoginView>

   

 <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
        SelectCommand="SELECT UserId, ApplicationId, UserName FROM Users"
    >
    </asp:SqlDataSource>
</asp:Content>
