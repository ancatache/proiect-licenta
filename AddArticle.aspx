﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddArticle.aspx.cs" Inherits="AddArticle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1"  Runat="Server" >

    <script>
        var x = document.getElementById("login");
        x.id = "login2"
    </script>

    <div style=" min-width:500px; background-image: url('bc3.png'); height: 1000px; width: 100%; ">

        <asp:LoginView ID="LVProfile" runat="server">
            <AnonymousTemplate>
                <div style="padding-top:350px">
                <div  class="addArticle" style="padding:50px">
                    This page has restricted access.
                    Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink> or sign in.
                </div>
                </div>
            </AnonymousTemplate>

              <RoleGroups>
            <asp:RoleGroup Roles="Editor">
                <ContentTemplate>
                <div style="padding-top:350px">
                <div class="addArticle" >
                    <table  style="font-family:Verdana;font-size:100%; border-spacing:20px; margin:0 auto">
                        <tr>
                            <td align="center" colspan="2" style="color:White;background-color:#C29590;font-weight:bold;margin-bottom:30px">Add New Paper</td>
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="ConferenceLabel" runat="server" Text="Conference:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Conference" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConferenceRequired" runat="server" ControlToValidate="Conference" ErrorMessage="Conference is required." ToolTip="Conference is required." ValidationGroup="Add">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="YearLabel" runat="server" Text="Year:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DDLYear" runat="server" DataSource="<%# Last100Years %>"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="PDFLabel" runat="server" Text="PDF:"></asp:Label>
                            </td>
                            <td>
                                <asp:FileUpload ID="FileUpload" runat="server" ToolTip="Select Only PDF File" />
                            </td>
                        </tr>
                     
                    </table>
  
                    <div style="padding-top: 10px">
                        <asp:Button class="redButton" ID="BAddArticle" runat="server" Text="Add " OnClick="AddArticle_Button" ValidationGroup="Add" />
                    </div>

                    <div style="padding: 10px">
                        <asp:Literal ID="LAnswer" runat="server"></asp:Literal>
                    </div>
 
                </div>
                </div>

            </ContentTemplate>
            </asp:RoleGroup>
                   <asp:RoleGroup Roles="User">
                <ContentTemplate>
                <div style="padding-top:350px">
                <div class="addArticle" >
                    <table  style="font-family:Verdana;font-size:100%; border-spacing:20px; margin:0 auto">
                        <tr>
                            <td align="center" colspan="2" style="color:White;background-color:#C29590;font-weight:bold;margin-bottom:30px">Add New Paper</td>
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="ConferenceLabel" runat="server" Text="Conference:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="Conference" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConferenceRequired" runat="server" ControlToValidate="Conference" ErrorMessage="Conference is required." ToolTip="Conference is required." ValidationGroup="Add">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="YearLabel" runat="server" Text="Year:"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="DDLYear" runat="server" DataSource="<%# Last100Years %>"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="PDFLabel" runat="server" Text="PDF:"></asp:Label>
                            </td>
                            <td>
                                <asp:FileUpload ID="FileUpload" runat="server" ToolTip="Select Only PDF File" />
                            </td>
                        </tr>
                     
                    </table>
  
                    <div style="padding-top: 10px">
                        <asp:Button class="redButton" ID="BAddArticle" runat="server" Text="Add " OnClick="AddArticle_Button" ValidationGroup="Add" />
                    </div>

                    <div style="padding: 10px">
                        <asp:Literal ID="LAnswer" runat="server"></asp:Literal>
                    </div>
 
                </div>
                </div>

            </ContentTemplate>
            </asp:RoleGroup>
                  
                   <asp:RoleGroup Roles="Admin">
                <ContentTemplate>
                    <div style="padding-top:350px">
                <div  class="addArticle" style="padding:50px">
                    This page has restricted access.
                    Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink>.
                </div>
                
            </div>
                     </ContentTemplate>
            </asp:RoleGroup>

        </RoleGroups>

        </asp:LoginView>

       
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
            SelectCommand="SELECT UserId, ApplicationId, UserName FROM Users">
        </asp:SqlDataSource>
    </div>
</asp:Content>

