﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class ArticleView : System.Web.UI.Page
{
    public int numVal;
    string idart = "";
    public HyperLink h1, h2;
    protected void Page_Load(object sender, EventArgs e)
    {
      
        string path = HttpContext.Current.Request.Url.Query;
        idart = path.Substring(1, path.Length - 1);
        numVal = Int32.Parse(idart);
        PDFfileHelper pdfHelper = PDFfileHelper.getInstance();
    

        h1 = ((HyperLink)(this.LoginView1.FindControl("HyperLinkD")));
        if (h1 != null )
        {
            h1.NavigateUrl = "~/DeleteArticle.aspx?" + numVal+"@";
        }

        h2 = ((HyperLink)(this.LoginView1.FindControl("HyperLink1")));
        if (h2 != null)
        {
            h2.NavigateUrl = "~/OpenPDF.aspx?" + numVal;
        }


        //   ArticleImg1.Src = "~/Images/" + idart + ".jpg";

        SintagmHelper sintagHelper = SintagmHelper.getInstance();
        List<Sintagm> sintagms = sintagHelper.findSintagmByIdArt(numVal);

        ArticleHelper articleHelper = ArticleHelper.getInstance();
        Article article = articleHelper.getArticleById(numVal);
        if(article!=null)
        {
            String yr = article.year.ToString();
            Label1.Text = article.title;
            Label4.Text = "(" + yr + " - "+article.category+")";
        }
        for (int i = 0; i < sintagms.Count; i++)
            Label5.Text = Label5.Text + "►" + sintagms[i].content+" ";
        Label3.Text = "";
        CommentHelper commentHelper = CommentHelper.getInstance();
        List<Comment> Comments = commentHelper.getAllCommentsByIdArt(numVal);
        if (Comments != null)
        {
            foreach (Comment comment in Comments)
                Label3.Text = Label3.Text + "<br /><br />" + comment.content;
        }
   //     Label4.Text = article.getMostPopularSintagm(article.getSintagms()).content + " " + article.getMostPopularSintagm(article.getSintagms()).number + " " + article.getMostPopularSintagm(article.getSintagms()).tfidf;
        
    }

   

    protected void addcom(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                LAnswer.Text = "You Must be Logged In to Post a Comment.";
                
            }
            else
            {
                try
                {
                    string name = Page.User.Identity.Name;
                    string content = name + ": " + TextArea1.Value;

                    Comment comment = new Comment(-1, numVal, content);
                    CommentHelper commentHelper = CommentHelper.getInstance();
                    String response = commentHelper.addComment(comment);
                    if (response.Equals(""))
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    else
                        LAnswer.Text = response;
                }

                catch (SqlException se)
                {
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {

                        LAnswer.Text = "Database connexion error : " + se.Message;
                    }
                }
                catch (Exception ex)
                {
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {

                        LAnswer.Text = "Data conversion error : " + ex.Message;
                    }
                }
            }
        }
    }

}