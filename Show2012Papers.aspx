﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Show2012Papers.aspx.cs" Inherits="Show2012Papers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style=" min-width:500px; background-image: url('bc3.png'); background-attachment: fixed; height: auto; width: 100%; ">
        <div style="padding-top:350px; padding-bottom:100px;">
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <div class="tagCloud" id="tagCloud" runat="server">
                <asp:Label ID="Label2" runat="server" Text="Topics from 2012:" class="weight"></asp:Label>
                <br />
                <br />
            </div>
    <asp:Repeater  ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            <div class="articleContainer">
                    <asp:Image ID="Image1" runat="server" src="blogging.png" />
                    <asp:HyperLink style="color: #C29590; text-decoration:none; margin-left:10px; font-size: 100%;" ID="HyperLink3" NavigateUrl='<%#"~/ArticleView.aspx?"+ Eval("Id") %>' runat="server"><%#Eval("title") %></asp:HyperLink>
                    
                    <asp:Label ID="LabelYear" runat="server" Text='<%#"("+Eval("year")+" - " %>'></asp:Label>
                   
                    <asp:Label ID="LabelC" runat="server" Text='<%#""+Eval("category")+")" %>'></asp:Label>
               
            </div>
        </ItemTemplate>
    </asp:Repeater>
            </div>
        </div>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
        SelectCommand="SELECT Id, title, content, year, content, category,description FROM Article where published=1 and year=2012 order by title asc">
    </asp:SqlDataSource>
   
</asp:Content>

