﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ArticleRequest.aspx.cs" Inherits="ArticleRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div style="overflow:auto">
    <asp:LoginView ID="LVProfile" runat="server">

        <AnonymousTemplate>
            <div style="background-image: url('bc3.png'); background-attachment: fixed; height: 1000px; width: 100%;">
            <div style="padding-top:350px">
            <div  class="addArticle" style="padding:50px">
                This page has restricted access.
                Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink> or sign in.
            </div>
            </div>
            </div>
        </AnonymousTemplate>
        
        <RoleGroups>
            <asp:RoleGroup Roles="Editor">
                <ContentTemplate>
                    <div style="background-image: url('bc3.png'); background-attachment: fixed; height: 10000px; width: 100%;">
                    <div style="padding-top:350px; padding-bottom:100px;">
                        
                        <div style="margin-left:500px;">
                            <asp:Literal ID="LAnswer" runat="server"></asp:Literal>
                        </div>
                        <br />
                        <br />
                        
                        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
                            <ItemTemplate>      
                                <div style="width: 50%; margin-left:150px; padding:30px;  background: rgba(255, 255, 255, 0.7); box-shadow: 2px 2px 5px #888888;" id="showArticles" runat="server">
                                <div style="display:block; padding: 2px;  ">
                                    <asp:HyperLink style="color:#C29590 ; text-decoration:none; font-weight:bold" ID="HyperLink3" NavigateUrl='<%#"~/ArticleView.aspx?"+ Eval("Id") %>' runat="server"><%#Eval("title") %></asp:HyperLink>
                                    <br /><br />
                                    <asp:Label ID="LabelYear" runat="server" Text='<%#"("+Eval("year")+" - " %>'></asp:Label>
                    
                                    <asp:Label ID="LabelC" runat="server" Text='<%#Eval("category")+")" %>'></asp:Label>
                                    <br /><br />
                                </div>
             
                                <div style="display: block;">
                                    <asp:Label ID="Label1" runat="server" Text="Approve:"></asp:Label>
                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                    <br /><br />
                                        <asp:HyperLink style="color: #F9766E; text-decoration: none;" ID="HyperLinkD" runat="server" NavigateUrl='<%#"~/DeleteArticle.aspx?"+ Eval("Id") %>' >Delete</asp:HyperLink>      
                
                                </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>

                        
                        <br />
                        <div style="margin-left:500px">
                            <asp:Button ID="Button1" runat="server" Text="Save" onClick="add_art" CssClass="redButton"/>   
                        
                        </div>

                           

                    </div>
                    </div>
               
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
       
        <LoggedInTemplate>
            <div style="background-image: url('bc3.png'); background-attachment: fixed; height: 1000px; width: 100%;">
                <div style="padding-top:350px">
                <div  class="addArticle" style="padding:50px">
                    This page has restricted access.
                    Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink> or sign in as editor.
                </div>
                
            </div>
                </div>
        </LoggedInTemplate>

    </asp:LoginView>

    
   
    </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
    SelectCommand="SELECT Id, title, content, year, description, category FROM Article where published=0">
    </asp:SqlDataSource>
    
</asp:Content>

