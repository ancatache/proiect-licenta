﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Categories : System.Web.UI.Page
{
    public int numVal;
    protected void Page_Load(object sender, EventArgs e)
    {
        string path = HttpContext.Current.Request.Url.Query;
        string idart = path.Substring(1, path.Length - 1);
        numVal = Int32.Parse(idart);
        SqlDataSource2.SelectCommand = "SELECT Article.URL as URL, Article.Id as Id, Article.title as title, Article.year as year, Article.description as description, Article.category as category FROM Article INNER JOIN Category on Category.Id=Article.IdCat where published=1 and Article.URL<>'' and Category.Id=" + numVal;
        SqlDataSource1.SelectCommand = "SELECT Article.Id as Id, Article.title as title, Article.year as year, Article.description as description, Article.category as category FROM Article INNER JOIN Category on Category.Id=Article.IdCat where published=1 and Article.URL='' and Category.Id=" + numVal;
    }

    protected void Sort(object sender, EventArgs e)
    {
        if (DropDownList1.SelectedValue.Equals("Year"))
        {
            SqlDataSource1.SelectCommand = "SELECT Article.Id as Id, Article.title as title, Article.year as year, Article.description as description, Article.category as category FROM Article INNER JOIN Category on Category.Id=Article.IdCat where published=1 and Article.URL='' and Category.Id=" + numVal + " ORDER BY year desc";
            SqlDataSource2.SelectCommand = "SELECT Article.URL as URL, Article.Id as Id, Article.title as title, Article.year as year, Article.description as description, Article.category as category FROM Article INNER JOIN Category on Category.Id=Article.IdCat where published=1 and Article.URL<>'' and Category.Id=" + numVal + " ORDER BY year desc";
        }
        else
            if (DropDownList1.SelectedValue.Equals("Alphabetically"))
            {
                SqlDataSource1.SelectCommand = "SELECT Article.Id as Id, Article.title as title, Article.year as year, Article.description as description, Article.category as category FROM Article INNER JOIN Category on Category.Id=Article.IdCat where published=1 and Article.URL='' and Category.Id=" + numVal + " ORDER BY Article.title";
                SqlDataSource2.SelectCommand = "SELECT Article.URL as URL, Article.Id as Id, Article.title as title, Article.year as year, Article.description as description, Article.category as category FROM Article INNER JOIN Category on Category.Id=Article.IdCat where published=1 and Article.URL<>'' and Category.Id=" + numVal + " ORDER BY Article.title";
            }
    }
}