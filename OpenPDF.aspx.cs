﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OpenPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string path = HttpContext.Current.Request.Url.Query;
        string idart = path.Substring(1, path.Length - 1);
        int numVal = Int32.Parse(idart); 

        Response.Redirect("~/PDFs/"+numVal+".pdf");
    }
}