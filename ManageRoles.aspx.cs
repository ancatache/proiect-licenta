﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ManageRoles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Repeater repeater = LVProfile.FindControl("Repeater1") as Repeater;
        if(repeater != null)
            repeater.ItemDataBound += new RepeaterItemEventHandler(LoadComplete);
         
   }


    void LoadComplete(object sender, EventArgs e)
    {
        Repeater repeater = LVProfile.FindControl("Repeater1") as Repeater;
        Literal answer = LVProfile.FindControl("LAnswer") as Literal;

        if (repeater != null)
        {
            foreach (RepeaterItem item in repeater.Items)
            {

                string Role = "";
                string UserId = "";
                Label id = (Label)item.FindControl("LabelId");
                DropDownList ddl = (DropDownList)item.FindControl("DropDownList1");

                UserId = id.Text;
                string query = "SELECT * FROM UsersInRoles where UserId= @UserId ";
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\AspnetDB.mdf;Integrated Security=True");
                con.Open();
                try
                {
                    SqlCommand com = new SqlCommand(query, con);
                    com.Parameters.AddWithValue("UserId", UserId);

                    int d = com.ExecuteNonQuery();
                    var read = com.ExecuteReader();
                    if (read.HasRows)
                    {
                        while (read.Read())
                        {

                            if (read.GetGuid(1).ToString().Equals("ff377b68-db32-4094-b666-e4db6b426476"))
                                Role = "User";
                            else
                                if (read.GetGuid(1).ToString().Equals("323d5c51-d5d5-4767-bb26-5f07a360eb98"))
                                Role = "Admin";
                            else
                                    if (read.GetGuid(1).ToString().Equals("5da21c5a-f1be-4cf9-9aa3-c640b8415806"))
                                Role = "Editor";

                            ddl.SelectedValue = Role;

                        }
                    }
                    read.Close();

                }
                catch (Exception ex)
                {
                    if(answer != null)
                        answer.Text = "Database insert error : " + ex.Message;
                }
                finally
                {
                    con.Close();
                }





            }
        }
               
    }
    
    protected void SaveChanges(object sender, EventArgs e)
    {
        Literal answer = LVProfile.FindControl("LAnswer") as Literal;
        string queryDelete = "DELETE FROM UsersInRoles";
        SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Users\Sebi\Documents\proiect-licenta\App_Data\AspnetDB.mdf;Integrated Security=True");
        con.Open();
        try
        {
            SqlCommand com = new SqlCommand(queryDelete, con);

            int d = com.ExecuteNonQuery();
            answer.Text = "Data deleted!";

        }
        catch (Exception ex)
        {
            answer.Text = "Database error : " + ex.Message;
        }
        finally
        {
            con.Close();
        }
        Repeater repeater = LVProfile.FindControl("Repeater1") as Repeater;
        if (repeater != null)
        {
            foreach (RepeaterItem item in repeater.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    string RoleId = "ff377b68-db32-4094-b666-e4db6b426476";
                    string UserId = "";
                    Label id = (Label)item.FindControl("LabelId");
                    DropDownList ddl = (DropDownList)item.FindControl("DropDownList1");

                    UserId = id.Text;
                    if (ddl.SelectedValue.Equals("User"))
                        RoleId = "ff377b68-db32-4094-b666-e4db6b426476";
                    else
                        if (ddl.SelectedValue.Equals("Admin"))
                        RoleId = "323d5c51-d5d5-4767-bb26-5f07a360eb98";
                    else
                            if (ddl.SelectedValue.Equals("Editor"))
                        RoleId = "5da21c5a-f1be-4cf9-9aa3-c640b8415806";

                    string query = "INSERT INTO UsersInRoles (UserId, RoleId)"
                     + " VALUES (@UserId, @RoleId)";

                    con.Open();
                    try
                    {
                        SqlCommand com = new SqlCommand(query, con);
                        com.Parameters.AddWithValue("UserId", UserId);
                        com.Parameters.AddWithValue("RoleId", RoleId);
                        int d = com.ExecuteNonQuery();
                        answer.Text = "Data saved!";

                    }
                    catch (Exception ex)
                    {
                        answer.Text = "Database insert error : " + ex.Message;
                    }
                    finally
                    {
                        con.Close();
                    }



                }
            }
        }

            

        }
    }

