﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyPapers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        ArticleHelper articleHelper = ArticleHelper.getInstance();
        List<Article> articles = articleHelper.getAllArticlesByUserName(Page.User.Identity.Name);
        if (articles != null)
        {
            for (int j = 0; j < articles.Count; j++)
            {
                HyperLink hypLink = new HyperLink();
                hypLink.Text = articles[j].title + "</br>" + "(" + articles[j].year + " - " + articles[j].category + ")" + "<br /><br />";
                hypLink.NavigateUrl = "~/ArticleView.aspx?" + articles[j].Id;
                hypLink.CssClass = "hiper";
                showArticles.Controls.Add(hypLink);
            }
        }
        
  
    }
}