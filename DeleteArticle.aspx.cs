﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;

public partial class DeleteArticle : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int ok = 0;
        string path = HttpContext.Current.Request.Url.Query;
        if (path[path.Length - 1].CompareTo('@') == 0)
        {
            ok = 1;
            path = path.Remove(path.Length - 1, 1);
        }
        string idart = path.Substring(1, path.Length - 1);
        int numVal = Int32.Parse(idart);

        File.Delete(Server.MapPath("~") + "/Images/" + numVal + ".jpg");

        File.Delete(Server.MapPath("~") + "/PDFs/" + numVal + ".pdf");

        CommentHelper commentHelper = CommentHelper.getInstance();
        String responseC = commentHelper.deleteAllCommentsByIdArt(numVal);

        PDFfileHelper pdfHelper = PDFfileHelper.getInstance();
        String responseP = pdfHelper.deletePDFByIdArt(numVal);

        ArticleHelper articleHelper = ArticleHelper.getInstance();
        String response = articleHelper.deleteArticle(numVal);
        if(response.Equals("")==true&&ok==0)
            Response.Redirect("ArticleRequest.aspx", false);
        else
            if(response.Equals("") == true && ok == 1)
                Response.Redirect("ShowPapers.aspx", false);

    }
}