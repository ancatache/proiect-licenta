﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
            SintagmHelper sintagmHelper = SintagmHelper.getInstance();
        //   sintagmHelper.deleteAllSintagms();

        /*  Dictionary<String, List<int>> voc = WordUtils.calculateVocabular();
          ArticleHelper articleHelper1 = ArticleHelper.getInstance();
          List<Article> articles = articleHelper1.getAllArticles();
          for (int i = 0; i < articles.Count; i++)
          {
              Dictionary<String, Double> tfidf_values = WordUtils.calculateTFIDF(i, voc);
              Dictionary<String, Double> twoWordsSintagms = articles[i].getTwoWordsSintagms(tfidf_values);
              Dictionary<String, Double> threeWordsSintagms = articles[i].getThreeWordsSintagms(tfidf_values);
              Dictionary<String, Double> oneSingleDictionary = new Dictionary<String, Double>(); // combines the two dictionaries of two respective three words 
              twoWordsSintagms.ToList().ForEach(x => oneSingleDictionary.Add(x.Key, x.Value));
              threeWordsSintagms.ToList().ForEach(x => oneSingleDictionary.Add(x.Key, x.Value));
              Sintagm[] s = articles[i].getMostPopularThreeSintagms(oneSingleDictionary);

              BaseDBResponse response;
              for (int j = 0; j < s.Length; j++)
                  response = sintagmHelper.saveSintagm(s[j]);
          }
          */
      //  ArticleHelper articleHelper = ArticleHelper.getInstance();
       // articleHelper.setUserNameFields();

      //  SintagmHelper sintagmHelper = SintagmHelper.getInstance();
        List<Sintagm> sintagms = sintagmHelper.getAllSintagmsOrdered();
        // Sintagm max = sintagms[0];
        ArrayUtils.Shuffle<Sintagm>(sintagms);
      
        for (int i = 0; i < sintagms.Count; i++)
        {
            if (!(sintagms[i].content.Equals("park carnegie") || sintagms[i].content.Equals("shgusdngogo hsseo") || sintagms[i].content.Equals("wagner faculty") || sintagms[i].content.Equals("wine zebra") || sintagms[i].content.Equals("pennsylvania chinese") || sintagms[i].content.Equals("technology tsinghua") || sintagms[i].content.Equals("mails tsinghua") || sintagms[i].content.Equals("mark dredze") || sintagms[i].content.Equals("curved beak") || sintagms[i].content.Equals("rateo logn") || sintagms[i].content.Equals("phrase pair") || sintagms[i].content.Equals("soufiani seas") || sintagms[i].content.Equals("strategy nneg") || sintagms[i].content.Equals("wikipedia articles")))
            {
                if (sintagms[i].tfidf >= 0 && sintagms[i].tfidf < 0.02)
                {
                    HyperLink hypLink = new HyperLink();
                    hypLink.CssClass = "weight0";
                    hypLink.Text = sintagms[i].content + " • ";
                    hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                    tagCloud.Controls.Add(hypLink);
                }
                // Label2.Text += "<h class='weight1'>" + " " + sintagms[i].content + " " + "</h> • ";
                else if (sintagms[i].tfidf < 0.03 && sintagms[i].tfidf >= 0.02)
                {
                    HyperLink hypLink = new HyperLink();
                    hypLink.CssClass = "weight1";
                    hypLink.Text = sintagms[i].content + " " + " • ";
                    hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                    tagCloud.Controls.Add(hypLink);
                }
                //   Label2.Text += "<h class='weight2'>" + " " + sintagms[i].content + " " + "</h>  •";
                else if (sintagms[i].tfidf >= 0.03 && sintagms[i].tfidf < 0.04)
                {
                    HyperLink hypLink = new HyperLink();
                    hypLink.CssClass = "weight2";
                    hypLink.Text = sintagms[i].content + " " + " • ";
                    hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                    tagCloud.Controls.Add(hypLink);
                }
                // Label2.Text += "<h class='weight3'>" + " " + sintagms[i].content + " " + "</h>  •";
                else if (sintagms[i].tfidf <= 0.05 && sintagms[i].tfidf >= 0.04)
                {
                    HyperLink hypLink = new HyperLink();
                    hypLink.CssClass = "weight3";
                    hypLink.Text = sintagms[i].content + " " + " • ";
                    hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                    tagCloud.Controls.Add(hypLink);
                }

            }

        }
    }
}
    

    


    

    

