﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Show2008Papers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        System.Web.UI.HtmlControls.HtmlGenericControl currdiv = (System.Web.UI.HtmlControls.HtmlGenericControl)Master.FindControl("header").FindControl("profile");
        currdiv.Style.Add("display", "none");

        TopicHelper topicHelper = TopicHelper.getInstance();
        List<Topic> topics = topicHelper.get2008Topics();


        for (int i = 0; i < topics.Count; i++)
        {

            HyperLink hypLink = new HyperLink();
            hypLink.CssClass = "weight" + i % 3;
            hypLink.Text = topics[i].content + " • ";
            //  hypLink.NavigateUrl = "~/ShowArticles.aspx?" + topics[i].content;
            tagCloud.Controls.Add(hypLink);
        }

        /* List<Sintagm> sintagms = new List<Sintagm>();
         SintagmHelper sintagmHelper = SintagmHelper.getInstance();
         ArticleHelper articleHelper = ArticleHelper.getInstance();
         List<Article> articles = articleHelper.getAllArticlesByYear(2008);
         for (int i = 0; i < articles.Count; i++)
             sintagms.AddRange(sintagmHelper.findSintagmByIdArt(articles[i].Id));
         ArrayUtils.Shuffle<Sintagm>(sintagms);
         for (int i = 0; i < sintagms.Count; i++)
         {
             if (sintagms[i].tfidf < 0.05 && sintagms[i].tfidf >= 0)
             {
                 HyperLink hypLink = new HyperLink();
                 hypLink.CssClass = "weight0";
                 hypLink.Text = sintagms[i].content + " • ";
                 hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                 tagCloud.Controls.Add(hypLink);
             }

             else if (sintagms[i].tfidf >= 0.05 && sintagms[i].tfidf < 0.1)
             {
                 HyperLink hypLink = new HyperLink();
                 hypLink.CssClass = "weight1";
                 hypLink.Text = sintagms[i].content + " " + " • ";
                 hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                 tagCloud.Controls.Add(hypLink);
             }

             else if (sintagms[i].tfidf < 0.15 && sintagms[i].tfidf >= 0.1)
             {
                 HyperLink hypLink = new HyperLink();
                 hypLink.CssClass = "weight2";
                 hypLink.Text = sintagms[i].content + " " + " • ";
                 hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                 tagCloud.Controls.Add(hypLink);
             }

             else if (sintagms[i].tfidf < 0.2 && sintagms[i].tfidf >= 0.15)
             {
                 HyperLink hypLink = new HyperLink();
                 hypLink.CssClass = "weight3";
                 hypLink.Text = sintagms[i].content + " " + " • ";
                 hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                 tagCloud.Controls.Add(hypLink);
             }

             else
             if (sintagms[i].tfidf < 0.5 && sintagms[i].tfidf >= 0.2)
             {
                 HyperLink hypLink = new HyperLink();
                 hypLink.CssClass = "weight4";
                 hypLink.Text = sintagms[i].content + " " + " • ";
                 hypLink.NavigateUrl = "~/ShowArticles.aspx?" + sintagms[i].content;
                 tagCloud.Controls.Add(hypLink);
             }
         }*/
    }
}