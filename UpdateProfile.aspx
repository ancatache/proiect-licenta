﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UpdateProfile.aspx.cs" Inherits="UpdateProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script>
        var x = document.getElementById("login");
        x.id = "login2"
    </script>

     <div style=" min-width:500px; background-image: url('bc3.png'); height: 1000px; width: 100%; ">

        <asp:LoginView ID="LVProfile" runat="server">
        
            <AnonymousTemplate>
                <div style="padding-top:350px">
                    <div  class="addArticle" style="padding:50px">
                        This page has restricted access.
                        Return to <asp:HyperLink style="text-decoration:none; color:#C29590" ID="HyperLink1" NavigateUrl="~/Home.aspx" runat="server">Home</asp:HyperLink> or sign in.
                    </div>
                </div>
            </AnonymousTemplate>
               
            <LoggedInTemplate>
                <div style="padding-top:350px">
                <div  class="addArticle">
                    <table style="font-family:Verdana;font-size:100%; border-spacing:20px; margin:0 auto">
                        <tr>
                            <td align="center" colspan="2" style="color:White;background-color:#C29590;font-weight:bold;margin-bottom:100px">Update Profile</td>
                        </tr>
                        <tr>
                            
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label2" runat="server" Text="First Name:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TBFirstName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TBFirstName" runat="server" ErrorMessage="First name is required" ForeColor="#EF726B">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label6" runat="server" Text="Last Name:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TBLastName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="TBLastName" runat="server" ErrorMessage="Last name is required" ForeColor="#EF726B">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label7" runat="server" Text="Birthday:"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TBBirthday" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="TBBirthday" runat="server" ErrorMessage="Birthday is required" ForeColor="#EF726B" >*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TBBirthday" ErrorMessage="Date must be in MM/DD/YYYY format" ForeColor="#EF726B" Type="Date" Operator="DataTypeCheck" Display="Dynamic"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="Label3" runat="server" Text="Gender:"></asp:Label>
                            </td>
                            <td>
                                
                                <asp:RadioButtonList ID="RBLGender" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Female" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>    
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="RBLGender" runat="server" ErrorMessage="Gender is required" ForeColor="#EF726B" Display="Dynamic"></asp:RequiredFieldValidator>  
                                     
                            </td>
                        </tr>
                     
                    </table>
            
            
          
                    <div style="padding: 50px">
                        <asp:Button ID="SaveButton" runat="server" Text="Save Profile" OnClick="SaveButton_Click" class="redButton" />
                        <br />
                        <br />

                        <asp:Literal ID="LMessage" runat="server"></asp:Literal>
                    </div>
                </div>
                </div>
            </LoggedInTemplate>

        </asp:LoginView>

         
    </div>
  
</asp:Content>

